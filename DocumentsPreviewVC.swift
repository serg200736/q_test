import UIKit

class DocumentsPreviewVC: PXLBaseVC {
	
	@IBOutlet private weak var tableView: UITableView!
	@IBOutlet private weak var movingView: MovingView!
	@IBOutlet private weak var safeAreaHelperView: UIView!
	
	private var sections = [Section]()
	private var placeholderIsVisible = false {
		didSet {
			handleMovingPossibility()
		}
	}
	private var isScrolling = false {
		didSet {
			handleMovingPossibility()
		}
	}
	
	// MARK: - ViewController livecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		fillSections()
		setupTableView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		movingView.enableRadioButtons()
		let value = UIInterfaceOrientation.landscapeRight.rawValue
		UIDevice.current.setValue(value, forKey: String.orientationConst)
		navigationController?.navigationBar.isHidden = false
		tableView.scrollRectToVisible(.zero, animated: false)
		movingView.resetButton()
		DocumentManager.shared.saveDoc = true
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tableView.flashScrollIndicators()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		if self.isMovingFromParentViewController {
			if PXLCurrentProgress.shared.errorStage != .counteAtemptsEmpty {
				DocumentManager.shared.previosStage(state: .removeLast)
			}
		}
		
		if self.isMovingFromParentViewController && PXLCurrentProgress.shared.progressStage == .videoCapture {
			PXLCurrentProgress.shared.previousStage()
		}
	}
	
	private func setupTableView() {
		tableView.delegate = self
		tableView.dataSource = self
		tableView.backgroundColor = .clear
		tableView.sectionHeaderHeight = UITableViewAutomaticDimension
		tableView.estimatedSectionHeaderHeight = 2
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 2
		tableView.sectionFooterHeight = UITableViewAutomaticDimension
		tableView.estimatedSectionFooterHeight = 2
	}
	
	private func fillSections() {
		let firstSections = createFirstSection()
		sections.append(firstSections)
		PXLUserModel.shared.docCountArray.enumerated().forEach { (index, _) in
			let cellData = PXLUserModel.shared.getPropertiesWithDescriptionAndValue(by: String.documentFieldTitles, documentNumber: index)
			let documentType = PXLUserModel.shared.allUserData.documentType[index]
			let documentSection = Section(cellsData: cellData, documentType: documentType)
			sections.append(documentSection)
		}
		
		let placeholderSection = Section(cellsData: [], documentType: nil)
		sections.append(placeholderSection)
	}
	
	private func createFirstSection() -> Section {
		let title = String.documentPreviewHeaderTitle
		let subtitle = PXLUserModel.shared.allUserData.fullName
		let section = Section(cellsData: [(title: title, subtitle: subtitle)], documentType: nil)
		return section
	}
	
	private func handleMovingPossibility() {
		movingView.canMove = !(placeholderIsVisible || isScrolling)
	}
	
	private func createBlankPlaceholderView(height: CGFloat) -> UIView {
		let placeholderView = UIView()
		placeholderView.backgroundColor = .clear
		placeholderView.translatesAutoresizingMaskIntoConstraints = false
		placeholderView.heightAnchor.constraint(equalToConstant: height).isActive = true
		return placeholderView
	}
	
	private func updateMovingView() {
		if movingView.isShown && !movingView.isMoving {
			movingView.hideAnimated(fast: true)
			return
		}
		
		guard placeholderIsVisible else { return }
		let headerViewRect = tableView.rectForHeader(inSection: sections.count - 1)
		let headerSuperviewYPosition = tableView.convert(headerViewRect, to: tableView.superview).origin.y
		let headerViewVisibleHeight = view.bounds.maxY - headerSuperviewYPosition - safeAreaHelperView.bounds.height
		let visiblePercent = headerViewVisibleHeight / (movingView.actionViewHeight)
		movingView.setHideableElementsAlpha(1 - visiblePercent)
		if !movingView.isShown {
			if visiblePercent > 1 {
				movingView.setNewOffset(headerViewVisibleHeight)
			} else {
				movingView.setHiddenOffset()
			}
		}
	}
	
	// MARK: - Actions
	@IBAction func continueTapped(_ sender: Any) {
		PXLCurrentProgress.shared.customerConfirmWrongData = movingView.selectedRadioButtonTag == 1
		if PXLVersionHelper.isAgentBuild {
			movingView.disableRadioButtons()
			movingView.startLoader()
			createArchive(completion: { (archiveURL) in
				self.performSegue(withIdentifier: .segueSendDataAgentConst, sender: archiveURL)
			}, onFail: {
				self.movingView.stopLoader()
				self.memoryAlert()
			})
		} else {
			PXLCurrentProgress.shared.nextStage()
			performSegue(withIdentifier: .segueShowTutorialsConst, sender: nil)
		}
	}
	
	@IBAction func rescanIDTapeed(_ sender: Any) {
		guard let scanDocumentDetailVC = navigationController?.viewControllers.first(where: { $0 is ScanDocumentDetailsVC }) else { return }
		DocumentManager.shared.previosStage(state: .removeAll)
		navigationController?.popToViewController(scanDocumentDetailVC, animated: true)
	}
	
	// MARK: - ViewController orientation
	override var shouldAutorotate: Bool {
		return true
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .portrait
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == .segueSendDataAgentConst, let sendingDataVC = segue.destination as? SendingDataVC, let archiveURL = sender as? URL {
			sendingDataVC.archiveURL = archiveURL
		}
	}
	
}

extension DocumentsPreviewVC: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if section == 0 {
			return tableView.dequeueReusableCell(withIdentifier: .storyboardTopHeaderIdentifierConst)
		} else {
			let currentSection = sections[section]
			if currentSection.isPlaseholder {
				return createBlankPlaceholderView(height: movingView.bounds.height)
			}
			let sectionHeader = tableView.dequeueReusableCell(withIdentifier: .storyboardSectionHeaderIdentifierConst) as? SectionHeader
			let stringNumber = String.getNumberText(for: section)
			let documentType = currentSection.documentType
			sectionHeader?.set(number: stringNumber, type: documentType)
			return sectionHeader
		}
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		if section != 0, !sections[section].isPlaseholder {
			let photoFooter = tableView.dequeueReusableCell(withIdentifier: .storyboardPhotoFooterIdentifierConst) as? PhotoFooter
			
			if section == 1 {
				let images = PXLCurrentProgress.shared.documentsImagesArray[0]
				photoFooter?.set(images: images)
			} else {
				let images = PXLCurrentProgress.shared.documentsImagesArray[1]
				photoFooter?.set(images: images)
			}
			return photoFooter
		}
		return createBlankPlaceholderView(height: 1)
	}
	
	func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		if sections[section].isPlaseholder {
			placeholderIsVisible = true
		}
	}
	
	func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
		if sections[section].isPlaseholder {
			placeholderIsVisible = false
		}
	}
	
}

extension DocumentsPreviewVC: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return sections.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sections[section].cellsData.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard
			let cell = tableView.dequeueReusableCell(withIdentifier: .storyboardTextCellIdentifierConst, for: indexPath) as? TextCell
			else { fatalError() }
		cell.set(cellData: sections[indexPath.section].cellsData[indexPath.row])
		return cell
	}
	
}

extension DocumentsPreviewVC {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		updateMovingView()
	}
	
	func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
		isScrolling = true
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		isScrolling = false
	}
	
}
