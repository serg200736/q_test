import UIKit
import WebKit

class WebViewPopupVC: UIViewController {
	@IBOutlet private weak var closeButton: UIButton!
	@IBOutlet private weak var webViewContainer: UIView!
	@IBOutlet private weak var pageDownloadProgressView: UIProgressView!
	@IBOutlet private weak var titleLabel: UILabel!
	@IBOutlet private weak var siteLabel: UILabel!
	@IBOutlet private weak var navAndStatusBars: NSLayoutConstraint!
	
	private let navigationBarHeight: CGFloat = 44
	private weak var hideWebViewObserver: NSObjectProtocol?
	private weak var webViewObserver: NSObjectProtocol?
	private var webView: WKWebView!
	private var token: NSKeyValueObservation?
	var url: URL?
	
	// MARK: - ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
		titleLabel.text = String.loadingTextConst
		siteLabel.text = url?.absoluteString
		navAndStatusBars.constant = navigationBarHeight + UIApplication.shared.statusBarFrame.height
		pageDownloadProgressView.progress = 0
		
		let center = NotificationCenter.default
		center.post(Notification(name: Notification.Name(rawValue: PXLNotifications.WebViewShown), object: self))
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		setupWebView()
		addObservers()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		let center = NotificationCenter.default
		center.post(Notification(name: Notification.Name(rawValue: PXLNotifications.WebViewClosed), object: self))
		removeObservers()
	}
	
	private func addObservers() {
		let center = NotificationCenter.default
		token = webView.observe(\.estimatedProgress, options: .new) { [weak self] (webView, _) in
			self?.pageDownloadProgressView.progress = Float(webView.estimatedProgress)
			if webView.estimatedProgress == 1 {
				UIView.animate(withDuration: .animationDuration, delay: 1, options: [], animations: {
					self?.pageDownloadProgressView.alpha = 0
				})
			} else {
				self?.pageDownloadProgressView.alpha = 1
			}
		}
		hideWebViewObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.HideWebView), object: nil, queue: nil) { _ in
			self.dismiss(animated: true)
		}
	}
	
	private func removeObservers() {
		let center = NotificationCenter.default
		
		if hideWebViewObserver != nil {
			center.removeObserver(hideWebViewObserver!)
		}
		token = nil
	}

	private func setupWebView() {
		let webConfiguration = WKWebViewConfiguration()
		webView = WKWebView(frame: webViewContainer.bounds, configuration: webConfiguration)
		webView.navigationDelegate = self
		webViewContainer.insertSubview(webView, at: 0)
		guard let url = url else { return }
		let request = URLRequest(url: url)
		webView.load(request)
	}
	
	// MARK: - Actions
	@IBAction func closePopup(_ sender: Any) {
		dismiss(animated: true)
	}
	
}

extension WebViewPopupVC: WKNavigationDelegate {
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		if let url = webView.url {
			titleLabel.text = "Quickline " + NSLocalizedString("Help", comment: "Menu third row")
			siteLabel.text = url.absoluteString
		}
	}
}
