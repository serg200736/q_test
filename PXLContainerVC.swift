import UIKit

class PXLContainerVC: PXLBaseVC {
	private var deepLinkOpenedObserver: NSObjectProtocol?
	private var scanQRCodeSelectedObserver: NSObjectProtocol?
	private var enterTransactionCodeSelectedObserver: NSObjectProtocol?
	private var helpSelectedObserver: NSObjectProtocol?
	private var aboutSelectedObserver: NSObjectProtocol?
	private var contactSelectedObserver: NSObjectProtocol?
	private var termsSelectedObserver: NSObjectProtocol?
	//show/hide menu
	private var showObserver: NSObjectProtocol?
	private var hideObserver: NSObjectProtocol?
	//helpStatus
	private var webViewShownObserver: NSObjectProtocol?
	private var webViewClosedObserver: NSObjectProtocol?
	
	// MARK: - ViewController livecycle
	override func viewDidLoad() {
        super.viewDidLoad()
        setupStoryboard()
		addObservers()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.rightViewController?.view.isHidden = true
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
	}
    
    private func setupStoryboard() {
        let stackId = PXLCurrentProgress.shared.transactionCode == nil ? String.storyboardCreateIdStackConst : String.storyboardDeepLinkStackConst
        let storyboard = UIStoryboard(name: String.mainStoryboardNameConst, bundle: nil)
        let storyboardMenu = UIStoryboard(name: String.menuTVCStoryboardNameConst, bundle: nil)
        guard let mainNavigationController: UINavigationController =
            storyboard.instantiateViewController(withIdentifier: stackId) as? UINavigationController else {return}
        guard let menuViewController = storyboardMenu.instantiateViewController(withIdentifier: String.storyboardMenuTVCConst)
            as? PXLMenuTVC else {return}
		
		menuViewController.view.frame.origin = CGPoint(x: UIScreen.main.bounds.width, y: menuViewController.view.frame.origin.y)
		
		self.leftViewController = mainNavigationController
		self.rightViewController = menuViewController
    }

	var leftViewController: UIViewController? {
		willSet {
			if self.leftViewController != nil {
				if self.leftViewController!.view != nil {
					self.leftViewController!.view!.removeFromSuperview()
				}
				self.leftViewController!.removeFromParentViewController()
			}
		}

		didSet {
			self.view!.addSubview(self.leftViewController!.view)
			self.addChildViewController(self.leftViewController!)
		}
	}

	var rightViewController: PXLMenuTVC? {
		willSet {
			if self.rightViewController != nil {
				if self.rightViewController!.view != nil {
					self.rightViewController!.view!.removeFromSuperview()
				}
				self.rightViewController!.removeFromParentViewController()
			}
		}

		didSet {
			self.view!.addSubview(self.rightViewController!.view)
			self.addChildViewController(self.rightViewController!)
		}
	}

	var menuShown: Bool = false
	var webViewShown: Bool = false

	override var isNeedMenu: Bool {
		if let topVC = (self.leftViewController as? PXLMainNavigationController)?.topViewController {
			return topVC.isNeedMenu
		}
		return false
	}

	@IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
		if self.menuShown && isNeedMenu && !webViewShown {hideMenu()}
	}

	@IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
		if !self.menuShown && isNeedMenu && !webViewShown {showMenu()}
	}

	func showMenu() {
		if !self.menuShown {
            guard leftViewController != nil else {return}
			self.rightViewController?.view.isHidden = false
			self.rightViewController?.tableView.reloadData()
			UIView.animate(withDuration: 0.3, animations: {
                self.rightViewController?.view.frame.origin.x = 0
			}, completion: { _ in
				self.menuShown = true
                self.leftViewController?.view.subviews.forEach({ (view) in
					if !view.isMember(of: UINavigationBar.self) {
						view.isUserInteractionEnabled = false
					}
				})
			})
		} else {
			hideMenu()
		}
	}

	func hideMenu() {
        guard leftViewController != nil else {return}
		UIView.animate(withDuration: 0.3, animations: {
			self.rightViewController?.view.frame.origin.x = self.view.frame.maxX
		}, completion: { _ -> Void in
			self.menuShown = false
			self.rightViewController?.view.isHidden = true
			self.rightViewController?.tableView.allowsSelection = true
			self.leftViewController?.view.subviews.forEach({ (view) in 
				if !view.isMember(of: UINavigationBar.self) {
					view.isUserInteractionEnabled = true
				}
			})
		})
	}

	private func addObservers() {
		let center = NotificationCenter.default
		deepLinkOpenedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.DeepLinkOpened), object: nil, queue: nil) { _ in
			self.hideMenu()
			let center = NotificationCenter.default
			if self.webViewShown {
				center.post(Notification(name: Notification.Name(rawValue: PXLNotifications.HideWebView), object: self))
			}
			self.setupStoryboard()
		}
		scanQRCodeSelectedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.ScanQRCodeSelected), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		enterTransactionCodeSelectedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.EnterTransactionCodeSelected), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		helpSelectedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.HelpSelected), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		aboutSelectedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.AboutSelected), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		contactSelectedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.ContactSelected), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		termsSelectedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.TermsSelected), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		//show/hide menu
		showObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.ShowMenu), object: nil, queue: nil) { _ in
			self.showMenu()
		}
		hideObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.HideMenu), object: nil, queue: nil) { _ in
			self.hideMenu()
		}
		//webView status
		webViewShownObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.WebViewShown), object: nil, queue: nil) { _ in
			self.webViewShown = true
		}
		webViewClosedObserver = center.addObserver(forName:
		Notification.Name(rawValue: PXLNotifications.WebViewClosed), object: nil, queue: nil) { _ in
			self.webViewShown = false
		}
	}

	private func removeObservers() {
		let center = NotificationCenter.default
		if deepLinkOpenedObserver != nil {
			center.removeObserver(deepLinkOpenedObserver!)
		}
		if scanQRCodeSelectedObserver !=  nil {
			center.removeObserver(scanQRCodeSelectedObserver!)
		}
		if enterTransactionCodeSelectedObserver !=  nil {
			center.removeObserver(enterTransactionCodeSelectedObserver!)
		}
		if helpSelectedObserver !=  nil {
			center.removeObserver(helpSelectedObserver!)
		}
		if aboutSelectedObserver != nil {
			center.removeObserver(aboutSelectedObserver!)
		}
		if contactSelectedObserver != nil {
			center.removeObserver(contactSelectedObserver!)
		}
		if termsSelectedObserver != nil {
			center.removeObserver(termsSelectedObserver!)
		}
		if showObserver != nil {
			center.removeObserver(showObserver!)
		}
		if hideObserver != nil {
			center.removeObserver(hideObserver!)
		}
		if webViewShownObserver != nil {
			center.removeObserver(webViewShownObserver!)
		}
		if webViewClosedObserver != nil {
			center.removeObserver(webViewClosedObserver!)
		}
	}
	
	// MARK: - ViewController orientation
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		if let visibleVC = rightViewController {
			return visibleVC.supportedInterfaceOrientations
		}
		return super.supportedInterfaceOrientations
	}
	
	deinit {
		removeObservers()
	}
}

extension UIViewController {
	@objc var isNeedMenu: Bool {
		return false
	}
}
